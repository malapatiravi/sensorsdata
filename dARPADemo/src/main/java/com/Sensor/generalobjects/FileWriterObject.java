package com.Sensor.generalobjects;

import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by malz on 11/21/16.
 */
/*
This class is not used for writing and reading the file for sensor data.
 */
public class FileWriterObject {
    /*
    Initialization of local variables.
     */
    private String DataFolder = "UnnamedProject";
    private File root;// = new File(Environment.getExternalStorageDirectory(), DataFolder);
    private String Filename="Untitled.txt";
    private File DataFile;
    private BufferedWriter BufferedFile;

    private String TAG = "FWO";
    //Constructor for FileWriterObject which is not used in this project. Based on the requirement the
    // implementaiton can be written.
    public FileWriterObject()
    {

    }
    /*
    Constructor should be called with the File object.
     */
    public FileWriterObject(File FObject)
    {

        DataFile = FObject;
        try
        {
            //opening the buffered reader
            BufferedFile = new BufferedWriter(new FileWriter(DataFile, true));
        } catch (IOException e)	{	e.printStackTrace();}
    }
    /*
       Constructor should be called with the File object.
       if append is set then the data is apended with another file without creating the attributes again for feature file
    */
    public FileWriterObject(File FObject, boolean Append)
    {
        DataFile = FObject;
        try
        {
            BufferedFile = new BufferedWriter(new FileWriter(DataFile, Append));
        } catch (IOException e)	{	e.printStackTrace();}
    }
    /*
       Another constructor with file name and folder name based on the requirment the developer should decide
       which one is required.
    */
    public FileWriterObject(String Filename1, String Folder)
    {
        DataFolder = Folder;
        Filename = Filename1;
        root = new File(Environment.getExternalStorageDirectory(), DataFolder);
        if (!root.exists()) { root.mkdirs(); }
        DataFile = new File(root, Filename);
        try
        {
            BufferedFile = new BufferedWriter(new FileWriter(DataFile, true));
        } catch (IOException e)	{	e.printStackTrace();}
        //Log.i(TAG, "File is opened: "+DataFile.getAbsolutePath().toString());
    }
    /*
           Another constructor with file name and folder name based on the requirment the developer should decide
           which one is required.
    */
    public FileWriterObject(String Filename1, String Folder, boolean Append)
    {
        DataFolder = Folder;
        Filename = Filename1;
        root = new File(Environment.getExternalStorageDirectory(), DataFolder);
        if (!root.exists()) { root.mkdirs(); }
        DataFile = new File(root, Filename);
        try
        {
            BufferedFile = new BufferedWriter(new FileWriter(DataFile, Append));
        } catch (IOException e)	{	e.printStackTrace();}
    }
    /*
        Another constructor with file name and folder name based on the requirment the developer should decide
        which one is required.
    */
    public FileWriterObject(String CompleteFilename, boolean Append)
    {
        DataFolder = "";
        Filename = CompleteFilename;
        root = new File(Environment.getExternalStorageDirectory(), DataFolder);
        final boolean b = !root.exists();
        if (b) { root.mkdirs(); }
        DataFile = new File(root, Filename);
        try
        {
            BufferedFile = new BufferedWriter(new FileWriter(DataFile, Append));
        } catch (IOException e)	{	e.printStackTrace();}
    }

    protected void CreateFileWriterObject()
    {

    }
    /*
          Another constructor with file name and folder name based on the requirment the developer should decide
          which one is required.
   */
    protected void CreateFileWriterObject(File FO)
    {
        DataFile = FO;
        try
        {
            BufferedFile = new BufferedWriter(new FileWriter(DataFile, true));
        } catch (IOException e)	{	e.printStackTrace();}
    }
    /*
          Another constructor with file name and folder name based on the requirment the developer should decide
          which one is required.
   */
    protected void CreateFileWriterObject(File FO, boolean Append)
    {
        DataFile = FO;
        try
        {
            BufferedFile = new BufferedWriter(new FileWriter(DataFile, Append));
        } catch (IOException e)	{	e.printStackTrace();}
    }
    /*
    SaveData is called to store single line.
    */
    public void SaveData(String Data)
    {
        try
        {
            BufferedFile.write(Data);
            BufferedFile.newLine();
            BufferedFile.flush();
        } catch (IOException e)	{	e.printStackTrace();}
    }
    /*
        CloseWriter is called to close the file writer
    */
    public void CloseWriter()
    {
        try
        {
            BufferedFile.close();
        } catch (IOException e)	{	e.printStackTrace();}
    }
}
