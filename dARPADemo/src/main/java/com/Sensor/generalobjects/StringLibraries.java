package com.Sensor.generalobjects;

import java.util.ArrayList;

/**
 * Created by malz on 11/21/16.
 */
/*
All String based helper functions are implemented here.
In the later implementations the developers can move most of the string helpers here

 */
public class StringLibraries  {
    /*
     * The below function will convert the list of string into a single string by appending the
     * character as appender character
     *
     */
    public String getPath(ArrayList<String> list, char appender)
    {
        StringBuilder str=new StringBuilder();
        for(String s:list)
        {
            str.append(s+appender);
        }

        return str.toString();
    }
}
