package com.Sensor.maininterface;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.Sensor.authentication.WekaClassifierRemoval;
import com.Sensor.darpademo.R;
import com.Sensor.generalobjects.AndroidFileManager;
import com.Sensor.services.EEGFeatureService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;


/**
 * Created by malz on 11/25/16.
 */
/*
This module is not used in this project and hence not required. Comments will be provided later
 */
public class EEGFragment extends Fragment {
    private Button AuthBtn;
    private String TAG = "EEGFragment";
    private MainActivity CurrentActivity;
    private TextView UpdateMsgBox;
    private TextView Totallines;
    private String UserIDval="NA";
    private EditText UserIDView;
    private String currModeVal="";
    //Radio Group Variables
    private RadioGroup DemoMode;
    private Intent EEGService;
    private String folderPath;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView =  inflater.inflate(R.layout.sensors, container, false);
        CurrentActivity = (MainActivity)getActivity();
        AuthBtn = (Button)rootView.findViewById(R.id.AuthNotif);
        AuthBtn.setBackgroundColor(Color.RED);
        UpdateMsgBox=(TextView)rootView.findViewById(R.id.UpdateMessageBox);
        Totallines=(TextView)rootView.findViewById(R.id.TotalLines);
        Totallines.setText("0 Lines");
        Button StartBtn=(Button)rootView.findViewById(R.id.StartButton);
        Button StopBtn = (Button)rootView.findViewById(R.id.StopButton);
        EEGService = new Intent(getActivity(), EEGFeatureService.class);
        StartBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //Get the user id view
                UserIDView=(EditText)rootView.findViewById(R.id.UserID);
                //Get the UserIDValue
                UserIDval=UserIDView.getText().toString();
                if(UserIDView.length()<4)
                {

                    Toast.makeText(getActivity().getBaseContext(),"User ID can not be less than 4 charcters",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    LocalBroadcastManager.getInstance(CurrentActivity).registerReceiver(StatusBroadcastreceiver, new IntentFilter("StatusBroadcastreceiver"));
                    UserIDval=UserIDView.getText().toString();
                    DemoMode=(RadioGroup)rootView.findViewById(R.id.DemoMode);
                    int SelectMode=DemoMode.getCheckedRadioButtonId();
                    RadioButton ModeRVal = (RadioButton)rootView.findViewById(SelectMode);
                    String ModeVal =  ModeRVal.getText().toString();
                    currModeVal=ModeVal;
                    File curr_path= new File(Environment.getExternalStorageDirectory()+"/WatchEEG_Data/"+UserIDval+"/",getFileName(currModeVal));
                    File fol_path_temp=new File(Environment.getExternalStorageDirectory()+"/WatchEEG_Data/"+UserIDval+"/","");
                    folderPath=fol_path_temp.getAbsolutePath()+"/One/";
                    Log.i("EEG Sensor Path",curr_path.getAbsolutePath());
                    EEGService.putExtra("path",curr_path.getAbsolutePath());
                    if(getActivity().startService(EEGService)==null)
                    {
                        Log.i(TAG,"No Service is running in EEG");
                        Toast.makeText(getActivity().getBaseContext(),"No Background Service is running", Toast.LENGTH_SHORT).show();

                    }

                }
            }
        });
        StopBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                CurrentActivity.stopService(EEGService);
                doStopping();
                Log.i("Stopping","Service");
            }
        });
        return rootView;
    }
    private void doStopping()
    {

        if(currModeVal.equals("TrainingImp"))
        {
            combineImpAndGenu(folderPath);
            Log.i("Stopped","Caling stopping"+folderPath);
        }
        if(currModeVal.equals("Testing"))
        {
            doTesting(folderPath);
        }
    }
    /*
     * This function returns the correct required file Name
     */
    private String getFileName(String _modeVal)
    {
        String file_name="";
        if(_modeVal.equals("TrainingGenu"))
        {
            file_name="One/One_NS.txt";

        }
        else if(_modeVal.equals("TrainingImp"))
        {
            file_name="One/Two_NS.txt";
        }
        else if(_modeVal.equals("Testing"))
        {
            file_name="One/Three_NS.txt";
        }
        return file_name;
    }
    private BroadcastReceiver StatusBroadcastreceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String dataDisplay=intent.getStringExtra("data");
            Totallines.setText(dataDisplay);
        }
    };

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(CurrentActivity).registerReceiver(StatusBroadcastreceiver, new IntentFilter("StatusBroadcastreceiver"));
        CurrentActivity.stopService(EEGService);
        super.onDestroy();
    }
    private void combineImpAndGenu(String path)
    {
        try
        {
            Log.i("Combining","Combinint now"+path);
            BufferedReader dataImposterFile=new BufferedReader(new FileReader(new File(path,"Two_NS_Filter.arff")));
            BufferedReader dataGenuineFile=new BufferedReader(new FileReader(new File(path,"One_NS_Filter.arff")));
            BufferedWriter dataTrainingFile=new BufferedWriter(new FileWriter(new File(path,"TrainingAcc.arff")));
            //First read the Genuine File and write
            String aLine;
            while((aLine=dataGenuineFile.readLine())!=null)
            {
                dataTrainingFile.write(aLine);
                dataTrainingFile.newLine();
            }
            dataGenuineFile.close();
            boolean shallRead=false;
            while((aLine=dataImposterFile.readLine())!=null)
            {
                if(aLine.contains("@data"))
                {
                    shallRead=true;
                }
                if(!aLine.contains("@data")&&shallRead==true)
                {
                    dataTrainingFile.write(aLine);
                    dataTrainingFile.newLine();
                }

            }
            dataImposterFile.close();
            dataTrainingFile.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    private void doTesting(String path)
    {

        Log.i("Testing","Testing in the main"+path+"TestingAcc.txt");

        AndroidFileManager TrainingFile=new AndroidFileManager(path,currModeVal+"Acc.txt");
        try
        {
//            SensorsTemplateManagerR TrainingObject=new SensorsTemplateManagerR(TrainingFile, 3);
  //          TrainingObject.createArffFileForTesting();
            File trainingFile=new File(path,"TrainingAcc.arff");
            File testingFile=new File(path,"Three_NS_Filter.arff");
            //String new_path=Environment.getExternalStorageDirectory()+"/"+path;
            WekaClassifierRemoval decision=new WekaClassifierRemoval(trainingFile, testingFile, "/WatchEEG_Data/"+UserIDval+"/One/");
            int FeatureIndexesToBeRemoved[]={4,18,32,46};
            decision.setFeatureIndexesToBeRemoved(FeatureIndexesToBeRemoved);
            decision.setThreshold(0.80);
            boolean Decision=decision.getFinalDecision();
            if(Decision==true)
            {
                AuthBtn.setText("Authenticated and True");
                AuthBtn.setBackgroundColor(Color.GREEN);
            }
            else{
                AuthBtn.setText("Rejected and False");
                AuthBtn.setBackgroundColor(Color.RED);
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
