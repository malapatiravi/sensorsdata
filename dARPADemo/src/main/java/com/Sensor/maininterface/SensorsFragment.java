package com.Sensor.maininterface;

import android.annotation.SuppressLint;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.Sensor.authentication.SensorsTemplateManagerR;
import com.Sensor.authentication.WekaClassifierRemoval;
import com.Sensor.darpademo.R;

import com.Sensor.generalobjects.AndroidFileManager;
import com.Sensor.services.SensorsClass;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.List;

/**
 * Created by malz on 11/19/16.
 */
/*
 * This is core of the sensor data collection module. This module will get loaded from the MainActivity
 */
public class SensorsFragment extends Fragment {
    /*
     * The below are the local varioables that are required to work
     */
    private Button AuthBtn; // This represents the Authentication button
    private String TAG="SensorFrag"; //This for comments tag
    private Intent SensorsService; // To call SEnsors service which collects the data and store in the background
    private Intent SensorAuthService;
    private RadioGroup DemoMode; // This the radio button. When we select either Training Genuine, Training Imposter, Testing
                                // the status of the mode is updated in this message
    private EditText UserID; //User ID is stored in this field and can be ised to create the user specific implementation.
    List<String> DataFromMain;// No used in this project and ignore.

    private int NumberOfFeatures=23; //just a count of number of the feature but never mind as this is not used.
    private String CurrentFolder=""; //Not used in this proejct. This was used in the previous module
    private String CurrentFile="";//Not used in this proejct. This was used in the previous module
    private String Folder="DataFolder";// Data Folder where the data is stored.
    private String CurrentMode="Training"; // Initialization of current mode to Training but this is not used anywhere and
        // new initialization was present in the below code
    private String AuthorizedUser="Trainer"; //Not used in this proejct. This was used in the previous module
    private String TestingFilename="Testing.txt";//Not used in this proejct. This was used in the previous module
    private String TemplateFilename="Training.arff";//Not used in this proejct. This was used in the previous module
    //The below SensorFol define the folder in which the data to be stored.
    private String SensorFol="Sensors"; //The name of the module in this project
    private MainActivity CurrentActivity; //Current activity
    private String UserIDVal="NA"; //initialized userIDVa
    private TextView UpdateMsgBox; //Messagebox which displayes the status of data collection.
    private TextView TotalLines; //Total lines updated in the message box.
    private int NumberOfRequiredLine=1000; /// This is not used but can used to do meangful work like collecting minimum number of line of data is must.
    private String currModeVal=""; // The currMode value is used to store the value of the mode either Training or TEsting
    private boolean StopSensors=false; //If the sensor data collection is to be stopped or not.
    private String curr_file_path; // Current file path where the data file is stored.

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /*
         * On create view is called by default.
         * All the initialization variables are done in the below code.
         */
        CurrentActivity=(MainActivity)getActivity();
        final View rootView=inflater.inflate(R.layout.sensors, container, false);
        AuthBtn=(Button)rootView.findViewById(R.id.AuthNotif);
        AuthBtn.setBackgroundColor(Color.RED);
        AuthBtn.setText("Data Collection");
        UpdateMsgBox=(TextView)rootView.findViewById(R.id.UpdateMessageBox);
        TotalLines=(TextView)rootView.findViewById(R.id.TotalLines);
        TotalLines.setText("0 Lines");
        Button StartBtn=(Button)rootView.findViewById(R.id.StartButton);
        Button StopBtn=(Button)rootView.findViewById(R.id.StopButton);
        SensorsService=new Intent(getActivity(), SensorsClass.class);

        StartBtn.setOnClickListener(new View.OnClickListener()
        {
            /*
            When start button is clicked this function is called
             */
            @Override
            public void onClick(View view) {
                UpdateMsgBox.setText("Collecting Training Data... Please wait...\n");
                TotalLines.setText("Empty");
                UserID=(EditText)rootView.findViewById(R.id.UserID);
                if(UserID.length()<4)
                {
                    //If user id is not valid
                    Toast.makeText(getActivity().getBaseContext(),"User ID can not be empty",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    //If user id is valid
                    UserIDVal=UserID.getText().toString();
                    //Log.i(TAG,"User id = "+UserIDVal);
                    //Getting the root view
                    DemoMode=(RadioGroup)rootView.findViewById(R.id.DemoMode);
                    //Selected mode is read using below line of code
                    int SelectedMode = DemoMode.getCheckedRadioButtonId();
                    RadioButton ModeRVal = (RadioButton)rootView.findViewById(SelectedMode);
                    String ModeVal = ModeRVal.getText().toString();
                    currModeVal=ModeVal;
                    //Sending data by using the putExtra in the below code to SensorService
                    SensorsService.putExtra("DemoMode",ModeVal);
                    SensorsService.putExtra("SensorFol",SensorFol);
                    SensorsService.putExtra("UserID",UserIDVal);
                    AuthorizedUser=UserIDVal;
                    SensorsService.putExtra("Folder",Folder);
                    CurrentMode= ModeVal;
                    curr_file_path=Folder+"/"+UserIDVal+"/"+SensorFol+"/";
                    //Starting the sensor service now while starting the sensor service we make the auth button red
                    if(getActivity().startService(SensorsService)==null)
                    {

                        Log.i(TAG,"No Service is running at the moment.");
                        Toast.makeText(getActivity().getBaseContext(),"No BackGround Service is running",Toast.LENGTH_SHORT).show();
                    }
                  /*  else
                    {
                        if(!CheckStopMessageThread.isAlive())
                        {
                            CheckStopMessageThread.start();
                        }
                    }*/
                }
            }
        });
        /*
         * Stop button on click listener and this called when stop button is clicked.
         */
        StopBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                try
                {
                    StopLogging(curr_file_path);
                    UpdateMsgBox.setText("Training started and ended");
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
        return rootView;
    }

    /*
     * This thread checks if the stop command is issues or not and starts collecting the data.
     */
    Thread CheckStopMessageThread=new Thread()
    {
        @Override
        public void run() {
            boolean Temp=CurrentActivity.GotStopCommand();
            if(StopSensors != Temp)
            {
                Log.i(TAG,"Value is "+(CurrentActivity.GotStopCommand())+"and activity is: "+CurrentActivity.toString());
                try
                {
                    //StopLogging();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                StopSensors=false;
            }

        }
    };
    /*
     * The below function just stops the sensor logging service
     * further the function can be extended to stop various services independently.
     */
    private void StopLogging(String path) throws Exception
    {
        CurrentActivity.stopService(SensorsService);
        AuthBtn.setBackgroundColor(Color.RED);
        if(currModeVal.equals("TrainingGenu"))
        {
            doTrainingGenu(path);
        }
        else if(currModeVal.equals("TrainingImp"))
        {
            doTrainingImp(path);
        }
        else if(currModeVal.equals("Testing"))
        {
            doTesting(path);
        }
        //AuthBtn.setText("Authenticated"+currModeVal);
        //CurrentActivity.stopService(SensorAuthService);
        Log.i(TAG,"Stopping the sensor service");

    }
    private void doTesting(String path)
    {

        Log.i("Testing","Testing in the main"+path+"TestingAcc.txt");

        AndroidFileManager TrainingFile=new AndroidFileManager(path,CurrentMode+"Acc.txt");
        try
        {
            SensorsTemplateManagerR TrainingObject=new SensorsTemplateManagerR(TrainingFile, 3);
            TrainingObject.createArffFileForTesting();
            File trainingFile=new File(Environment.getExternalStorageDirectory()+"/"+path,"TrainingAcc.arff");
            File testingFile=new File(Environment.getExternalStorageDirectory()+"/"+path,"TestingAcc.arff");
            String new_path=Environment.getExternalStorageDirectory()+"/"+path;
            WekaClassifierRemoval decision=new WekaClassifierRemoval(trainingFile, testingFile, TrainingFile.GetCurrentFolder());
            int FeatureIndexesToBeRemoved[]={4,18,32,46};
            decision.setFeatureIndexesToBeRemoved(FeatureIndexesToBeRemoved);
            decision.setThreshold(0.80);
            boolean Decision=decision.getFinalDecision();
            if(Decision==true)
            {
                AuthBtn.setText("Authenticated and True");
                AuthBtn.setBackgroundColor(Color.GREEN);
            }
            else{
                AuthBtn.setText("Rejected and False");
                AuthBtn.setBackgroundColor(Color.RED);
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    /*
     * The below function does trianing for genuine user
     */
    private void doTrainingGenu(String path)
    {

        Log.i("Training","Training in the main"+path+"TrainingAcc.txt");
        AndroidFileManager TrainingFile=new AndroidFileManager(path,CurrentMode+"Acc.txt");
        try
        {
            SensorsTemplateManagerR TrainingObject=new SensorsTemplateManagerR(TrainingFile, 1);
            TrainingObject.createArffFileForTraining("Genuine");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }



    }
    /*
     * The below code does trianing for Imposter
     */
    private void doTrainingImp(String path)
    {
        Log.i("Training","Training in the main"+path+"TrainingAcc.txt");
        AndroidFileManager TrainingFile=new AndroidFileManager(path,CurrentMode+"Acc.txt");
        try
        {
            SensorsTemplateManagerR TrainingObject=new SensorsTemplateManagerR(TrainingFile, 2);
            TrainingObject.createArffFileForTraining("Imposter");
            combineImpAndGenu(path);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    /*
    The below code combines both the imposter and gennuine training data into singel file and
    label it to pass to weka classifier
     */
    private void combineImpAndGenu(String path)
    {
        Log.i("Combining:",Environment.getExternalStorageDirectory()+"/"+path+CurrentMode+"Acc.txt");
        //Combinig the Imposter and Genuine Files into Genuine File
        String imposterName = path+"TrainingImpAcc.txt";
        try
        {
            BufferedReader dataImposterFile=new BufferedReader(new FileReader(new File(Environment.getExternalStorageDirectory()+"/"+path,"TrainingImpAcc.arff")));
            BufferedReader dataGenuineFile=new BufferedReader(new FileReader(new File(Environment.getExternalStorageDirectory()+"/"+path,"TrainingGenuAcc.arff")));
            BufferedWriter dataTrainingFile=new BufferedWriter(new FileWriter(new File(Environment.getExternalStorageDirectory()+"/"+path,"TrainingAcc.arff")));
            //First read the Genuine File and write
            String aLine;
            while((aLine=dataGenuineFile.readLine())!=null)
            {
               dataTrainingFile.write(aLine);
                dataTrainingFile.newLine();
            }
            dataGenuineFile.close();
            boolean shallRead=false;
            while((aLine=dataImposterFile.readLine())!=null)
            {
                if(aLine.contains("@data"))
                {
                    shallRead=true;
                }
                if(!aLine.contains("@data")&&shallRead==true)
                {
                    dataTrainingFile.write(aLine);
                    dataTrainingFile.newLine();
                }

            }
            dataImposterFile.close();
            dataTrainingFile.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }



    }
}
