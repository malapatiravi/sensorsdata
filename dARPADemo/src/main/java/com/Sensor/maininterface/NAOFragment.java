package com.Sensor.maininterface;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.Sensor.darpademo.R;

/**
 * Created by malz on 11/19/16.
 */
/*
This is dummy and not used anywhere and hence can be ignored.
 */
public class NAOFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.nao, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
