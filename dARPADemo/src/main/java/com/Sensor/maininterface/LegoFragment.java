package com.Sensor.maininterface;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.Sensor.darpademo.R;
import com.Sensor.services.TouchClass;

import java.util.List;

/**
 * Created by malz on 11/19/16.
 */
/*
The below module is not used in this project and hence not providing any documentation for time being
However the code and the knowldge is transfered
 */
public class LegoFragment extends Fragment {
    private Button AuthBtn;
    private String TAG="LegoFrag";
    private Intent TouchService;
    private Intent AuthService;
    private RadioGroup DemoMode;
    //private String MainURL = "http://10.79.54.131/DEMO/savetodb.php";
    private EditText UserID;
   // private SwipeFeatureExtractor TrainingFE;
    List<String> DataFromMain;


    private int NumberOfFeatures = 23;
    private String CurrentFolder = "";
    private String CurrentFile = "";
    private String Folder = "DataFolder";
    private String CurrentMode = "Training";
    private String AuthorizedUser = "Ravi";
    private String TestingFilename = "Testing.txt";
    private String TemplateFilename = "TrainingTemplate.txt";
    private String DemoVal = "Lego";
    private MainActivity CurrentActivity;
    private boolean StopSwipe = false;

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        CurrentActivity = (MainActivity) getActivity();


        final View rootView = inflater.inflate(R.layout.lego, container, false);
        AuthBtn = (Button)rootView.findViewById(R.id.AuthNotif);
        Button StartBtn = (Button) rootView.findViewById(R.id.StartButton);
        Button StopBtn = (Button) rootView.findViewById(R.id.StopButton);
        TouchService = new Intent(getActivity(), TouchClass.class);

        /*
         * The Start button implementation is done here
         *
         */
        StartBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                UserID=(EditText)rootView.findViewById(R.id.UserID);
                String UserIDVal = UserID.getText().toString();
                if (UserID.length()<4)
                {
                    Toast.makeText(getActivity().getBaseContext(), "UserID cannot less than 4 characters!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    DemoMode = (RadioGroup)rootView.findViewById(R.id.DemoMode);
                    int SelectedMode= DemoMode.getCheckedRadioButtonId();
                    RadioButton ModeRVal = (RadioButton)rootView.findViewById(SelectedMode);
                    String ModeVal = ModeRVal.getText().toString();
                    TouchService.putExtra("DemoMode", ModeVal);
                    TouchService.putExtra("DemoFor", DemoVal);
                    TouchService.putExtra("UserID", UserIDVal);


                }
            }
        });

        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
