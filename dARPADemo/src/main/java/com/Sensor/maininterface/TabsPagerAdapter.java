package com.Sensor.maininterface;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

/**
 * Created by malz on 11/19/16.
 * This class is used to include all the tabs like NAO, LEgo and Sensors
 * In this project since we are using only Sensors we are going to use only Sensors Tab
 * and user will not be to use other fragments unless the Tabs string array is changes in the MainAvticity.java
 */

public class TabsPagerAdapter extends FragmentPagerAdapter {
	/*
	 * The Pageadapter is used to adapat the tabs in this project we have only one tab and
	 * we will not change much of the code in this File since this is already included in application
	 * where multiple sensors are included.
	 */
	public TabsPagerAdapter(FragmentManager fm) {
		/*
		Ignore this block of code
		 */
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {
		/*
		 * This is used to return the Fragment based on the case claock present in the below code.
		 * As of now we are using only sensorFragment and hence it is included in the case 0:
		 * if Swipe data collection is also need to be done then the commented code will be uncommented
		 */
		switch (index) {
		case 0:
			Log.i("Returning","NAO");
			return new SensorsFragment();
			//return new NAOFragment();
		case 1:
			Log.i("Returning","Lego");
			return new LegoFragment();
		case 2:
			Log.i("Returning","Sensor");
			return new SensorsFragment();
		case 3:
			Log.i("Returning"," EEG");
			return new EEGFragment();

		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 1;
	}

}
