package com.Sensor.services;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.widget.Toast;

import com.Sensor.generalobjects.AndroidFileManager;

import java.io.BufferedReader;
import java.io.File;

/**
 * Created by malz on 11/21/16.
 */

/*
 * The below code is for sensor authentication service.
 */
public class SensorsAuthService extends Service {
    /*
     * Defining the local variables required for all the member functions
     */

    private String TAG="SensorAuthService"; //TAG is used for logging
    private String Username; //Username
    private String DeviceFol; //The folder name is Sensors and is updated in the later sections
    private String Filename; // The File name is the fila name with which the data should be stored
    private String Folder; //The folder is the one which is base folder
    private String AuthorizedUser; // The authorised user is the name of the user who has access to the file
    private String TemplateFilename;
    private AndroidFileManager AFMObject;
    private File DataFile; //Data File objects
    private BufferedReader DataBuffer; //Data Buffer
    private boolean FileOpened = false; //Status of the file and this is not used in this project
    private int NumberOfRequiredLines=500; //Total minimum number of lines required not user in this project
    private boolean KeepLooping = true; //The status of the logging if set to false then the logging is done and not required anymore
    private long LastFileSize=0; //Not used in this project.
    private boolean IsAuthenticated=false; //Not used in this project
    private boolean FileLocked = false; //Not used in this project
    private String[] Message;//Not used in this project this is the message string which is used to store the data into the file
    private File root; //The root folder and this is not used in this projec.


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        /*
         * Once the activity or service is tarted then this is the function that is called by the Android operating system
         */
        Message=new String[10];
        Toast.makeText(getApplicationContext(), "Gait Authentication Started", Toast.LENGTH_SHORT).show();
        Username=intent.getExtras().getString("Username"); //Getting the passed messages
        DeviceFol=intent.getExtras().getString("DeciveFol"); //Getting the passed messages
        Filename=intent.getExtras().getString("Filename"); //Getting the passed messages
        Folder=intent.getExtras().getString("Folder"); //Getting the passed messages
        AuthorizedUser=intent.getExtras().getString("AuthorizedUser"); //Getting the passed messages
        TemplateFilename=intent.getExtras().getString("TemplateFilename"); //Getting the passed messages
        root=new File(Environment.getExternalStorageDirectory(),Folder);
        //AFMObject=new AndroidFileManager()
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
