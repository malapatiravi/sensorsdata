package com.Sensor.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.Sensor.generalobjects.EEGFileReader;
import com.Sensor.generalobjects.EEGFileWriter;

/**
 * Created by malz on 11/25/16.
 */
public class EEGFeatureService extends Service {
    /*
     *This will read a file and extract features and write them to a file in Arff format
     */

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
         /*
         * Once the activity or service is tarted then this is the function that is called by the Android operating system
         */

        String path=intent.getExtras().getString("path");

        EEGFileReader eegFileReader=new EEGFileReader(path);
        String folderPath=eegFileReader.getPathwithOutFileName();
        String writeFilePath=folderPath+getMatchingFilterFile(eegFileReader.getFileName());
        EEGFileWriter eegFilterFileWriter=new EEGFileWriter(writeFilePath);
        String aLine = eegFileReader.getNextLine();
        if(eegFileReader.getFileName().contains("One") || eegFileReader.getFileName().contains("Three"))
            createTrainingGenuArff(eegFilterFileWriter, eegFileReader.getFileName());
        else if(eegFileReader.getFileName().contains("Two"))
        {
            createTrainingImpArff(eegFilterFileWriter);
        }
        int count=0;
        String pLine="";
        while(aLine!="null")
        {

            if(aLine.contains("*"))
            {
                /*
                The below code excludes the comments in the data present and read only the data
                 */
                eegFilterFileWriter.eraseFile();
                pLine="";
                /*
                The below code checks if the activity is performed by the tester or genuine
                One indicates the TrainingGenuine
                Two indicates the TrainingImposter
                Three indicates the Testing
                 */
                if(eegFileReader.getFileName().contains("One") || eegFileReader.getFileName().contains("Three"))
                    createTrainingGenuArff(eegFilterFileWriter, eegFileReader.getFileName());
                else if(eegFileReader.getFileName().contains("Two"))
                {
                    createTrainingImpArff(eegFilterFileWriter);
                }
                count=0;
            }
            else if(aLine.contains("Raw"))
            {
                /*
                 * The below is the raw values based feature extraction where multiple raw valuese are kept
                 * as single row
                 * In this case 512 values are converted into 512 raw features.
                 * In conclusion there are 512 features which seems to be like worst case running time.
                 * as we are just doing testing project in pilot mode we want to
                 */
                if(count==511)
                {
                    pLine=pLine+getValue(aLine);
                    count=0;
                    if(eegFileReader.getFileName().contains("One") || eegFileReader.getFileName().contains("Three"))
                    {
                        eegFilterFileWriter.writeLineGenuine(pLine);
                    }
                    else if(eegFileReader.getFileName().contains("Two"))
                    {
                        eegFilterFileWriter.writeLineImposter(pLine);
                    }
                    pLine="";

                }
                else
                {
                    pLine=pLine+getValue(aLine)+",";
                    count++;
                }

            }
            aLine = eegFileReader.getNextLine();
        }
        eegFileReader.closeFile();
        eegFilterFileWriter.closeFile();
        Log.i("File Path is:",eegFileReader.getPathwithOutFileName());
        Intent inten=new Intent("StatusBroadcastreceiver");
        intent.putExtra("data","Done File work");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    public String getMatchingFilterFile(String _fileName)
    {
        if(_fileName.equals("One_NS.txt"))
        {
            return "One_NS_Filter.arff";
        }
        else if(_fileName.equals("Two_NS.txt"))
        {
            return "Two_NS_Filter.arff";
        }
        else if(_fileName.equals("Three_NS.txt"))
        {
            return "Three_NS_Filter.arff";
        }
        return null;
    }
    public String getValue(String aLine)
    {
        return aLine.substring(aLine.lastIndexOf(',')+1);
    }
    public void createTrainingGenuArff(EEGFileWriter trainArff, String file)
    {
        /*
        This function creates the Arff file for Genuine Training
        There are various functions that are required for each kind of user.
         */
        String firstLine="";
        if(file.contains("Two"))
        {
            firstLine="@relation TestingGenuarff";
        }
        else
        {
            firstLine="@relation TrainingGenuarff";
        }

        trainArff.writeLine(firstLine);
        trainArff.writeLine("");
        int i=0;
        while(i<512)
        {
            String aLine="@attribute raw"+i+" numeric";
            trainArff.writeLine(aLine);
            i++;
        }
        trainArff.writeLine("@attribute class {Genuine,Imposter}");
        trainArff.writeLine("");
        trainArff.writeLine("@data");

    }
    public void createTrainingImpArff(EEGFileWriter trainArff)
    {
        String firstLine="@relation TrainingImparff";
        trainArff.writeLine(firstLine);
        trainArff.writeLine("");
        int i=0;
        while(i<512)
        {
            String aLine="@attribute raw"+i+" numeric";
            trainArff.writeLine(aLine);
            i++;
        }
        trainArff.writeLine("@attribute class {Genuine,Imposter}");
        trainArff.writeLine("");
        trainArff.writeLine("@data");
    }
}
