package com.Sensor.services;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.Sensor.generalobjects.FileWriterObject;
import com.Sensor.generalobjects.StringLibraries;

import java.util.ArrayList;

/**
 * Created by malz on 11/19/16.
 */
@SuppressLint({"InlineApi","NewApi"})
public class SensorsClass extends Service implements SensorEventListener{
    //Creating the sensor manager reference message
    private SensorManager GlobalSensorManager;
    private Sensor LinearAccS;
    private Sensor RotationVectorS;
    private String CompleteData="";
    private int CurrentStatus=0;
    float[][] RMatrix = new float[3][3];
    public String LastData="";
    private int MAX_DATA_PACK=20;
    private FileWriterObject StoreObjectAcc;
    private FileWriterObject StoreObjectGyr;
    /*
     * The following variables are required to store the intent extras received from the SensorFragment
     * "CurrentMode" is either Training or Testing
     * "DeviceFol" is either Sensors or NAO or Lego in this file it is always refered as Sensors
     * "Folder" is always the DataFolder in which all the data is stored this is static and will not be changed this can also be hard coded
     * "UserID" is the user ud who is authorized to write the folder and this will always change
     *
     */
    private String CurrentMode="";
    private String DeviceFol="";
    private String Folder="";
    private String UserID="";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //get the information required for writing data file
         /*
         * Once the activity or service is tarted then this is the function that is called by the Android operating system
         */
        StringLibraries strLib=new StringLibraries();
        CurrentMode=intent.getExtras().getString("DemoMode");
        DeviceFol=intent.getExtras().getString("SensorFol");
        Folder=intent.getExtras().getString("Folder");
        UserID=intent.getExtras().getString("UserID");

        //Initializing the Sensor Manager
        GlobalSensorManager=(SensorManager)getSystemService(Context.SENSOR_SERVICE);
        //Initilizing the Accelerometer
        LinearAccS=GlobalSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        //Initilizing the Rotational Vector
        RotationVectorS=GlobalSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        //Registering the sensor listeners usinf the Global sensor Listener
        GlobalSensorManager.registerListener(GlobalSensorEventListener, LinearAccS, 30000);
        GlobalSensorManager.registerListener(GlobalSensorEventListener, RotationVectorS, 30000);
        Toast.makeText(getApplicationContext(),"Sensor logging started",Toast.LENGTH_SHORT).show();
        ArrayList<String> file_path=new ArrayList<String>();
        file_path.add(Folder);
        file_path.add(UserID);
        file_path.add(DeviceFol);
        Log.i(CurrentMode+".txt",strLib.getPath(file_path,'/'));
        StoreObjectAcc=new FileWriterObject(CurrentMode+"Acc.txt",strLib.getPath(file_path,'/'));
        StoreObjectGyr=new FileWriterObject(CurrentMode+"Gyr.txt",strLib.getPath(file_path,'/'));
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        GlobalSensorManager.unregisterListener(GlobalSensorEventListener);
        Toast.makeText(getApplicationContext(),"Sensor logging stopped",Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

    }

    /*
     * Defining new SensorEventListener which can read data and print
     */
    private SensorEventListener GlobalSensorEventListener=new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor TakenSensor=sensorEvent.sensor;
            //Log.i("SensorType is:",""+TakenSensor.getType());
            if(TakenSensor.getType()==Sensor.TYPE_LINEAR_ACCELERATION)
            {
                //Log.i("Values ACC:",""+sensorEvent.values[0]);
                //Log.i("Values ACC:",""+sensorEvent.values[1]);
                //Log.i("Values ACC:",""+sensorEvent.values[2]);
                String tempData=sensorEvent.values[0]+","+sensorEvent.values[1]+","+sensorEvent.values[2];
                StoreObjectAcc.SaveData(tempData);
            }
            else if(TakenSensor.getType()==Sensor.TYPE_ROTATION_VECTOR)
            {
                ///Log.i("Values ROT:",""+sensorEvent.values[0]);
                //Log.i("Values ROT:",""+sensorEvent.values[1]);
                //Log.i("Values ROT:",""+sensorEvent.values[2]);
                String tempData=sensorEvent.values[0]+","+sensorEvent.values[1]+","+sensorEvent.values[2];
                StoreObjectGyr.SaveData(tempData);

            }
        }


        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }


    };
}
