package com.Sensor.authentication;

import android.util.Log;

import com.Sensor.generalobjects.AndroidFileManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by malz on 11/22/16.
 */
/*
 * The below SensrosTemplateManagerR is used to create the feature file by using the Attribute list.
 * There are 56 features as shown below in the class.
 *
 */
public class SensorsTemplateManagerR {
    private static final String TAG="SensorTemplateManagerR";
    private int WindowSize, SlidingInterval;
    int StartIndex, EndIndex;
    private double FeatureMatrix[][];
    private double XSeries[], YSeries[], ZSeries[], MSeries[];
    private final int TotalFeatures=56;
    private String [][]AttributesList={
            {"apiX", "numeric"}, {"energyX", "numeric"} ,{"fQuantileX", "numeric"} ,{"kurtosisX", "numeric"} ,{"maxX", "numeric"} ,{"meanX", "numeric"},{"minX", "numeric"},
            {"nopeaksX", "numeric"} ,{"noZeroCrosingsX", "numeric"} ,{"powerX", "numeric"} ,{"skewnessX", "numeric"} ,{"sQuantileX", "numeric"},{"stdX", "numeric"},{"thQuantileX", "numeric"},
            {"apiY", "numeric"}, {"energyY", "numeric"} ,{"fQuantileY", "numeric"} ,{"kurtosisY", "numeric"} ,{"maxY", "numeric"} ,{"meanY", "numeric"},{"minY", "numeric"},
            {"nopeaksY", "numeric"} ,{"noZeroCrosingsY", "numeric"} ,{"powerY", "numeric"} ,{"skewnessY", "numeric"} ,{"sQuantileY", "numeric"},{"stdY", "numeric"},{"thQuantileY", "numeric"},
            {"apiZ", "numeric"}, {"energyZ", "numeric"} ,{"fQuantileZ", "numeric"} ,{"kurtosisZ", "numeric"} ,{"maxZ", "numeric"} ,{"meanZ", "numeric"},{"minZ", "numeric"},
            {"nopeaksZ", "numeric"} ,{"noZeroCrosingsZ", "numeric"} ,{"powerZ", "numeric"} ,{"skewnessZ", "numeric"} ,{"sQuantileZ", "numeric"},{"stdZ", "numeric"},{"thQuantileZ", "numeric"},
            {"apiM", "numeric"}, {"energyM", "numeric"} ,{"fQuantileM", "numeric"} ,{"kurtosisM", "numeric"} ,{"maxM", "numeric"} ,{"meanM", "numeric"},{"minM", "numeric"},
            {"nopeaksM", "numeric"} ,{"noZeroCrosingsM", "numeric"} ,{"powerM", "numeric"} ,{"skewnessM", "numeric"} ,{"sQuantileM", "numeric"},{"stdM", "numeric"},{"thQuantileM", "numeric"},

            {"class", "{Genuine,Imposter}"}};
    /*
     * The below two variables store the information of data file and the type of data file
     */
    private AndroidFileManager DataFile; //Stores the data file
    private int TrainGenuOrTrainImpOrTest;

    /*
     * Defining Feature file and ArffFile
     */
    private AndroidFileManager AndroidFeatureFile;
    private AndroidFileManager AndroidArffFile;
    /*
     * The below is the constructor where the datafile is provided as input and also information
     * about the file whether it is Training or Testing is informed.
     * if the value of _TrainGenuOrTrainImpOrTest is 0 then TrainGenuine
     * if the value is 1 then TrainImposter and if the value is 2 then the file is Testing file.
     * Based on the int value passed the file name will get created accordingly.
     *
     * The window size is defined as 200 as of now and this has to be changed in the below intialization variables WindowSize
     * Sliding interval is defined as 50 please refer the SlidingInterval in the below class
     */
    public SensorsTemplateManagerR(AndroidFileManager _DataFile, int _TrainGenuOrTrainImpOrTest)
            throws FileNotFoundException, IOException
    {
        this.TrainGenuOrTrainImpOrTest=_TrainGenuOrTrainImpOrTest;
        this.DataFile=_DataFile;
        /*
         * Create two  file one for storing features data and another one for storing arff after
         * extracting features
         */
        AndroidFeatureFile=new AndroidFileManager(DataFile.GetCurrentFolder(), DataFile.GetFilenameAlone()+"FeatureFile.txt");
        AndroidArffFile=new AndroidFileManager(DataFile.GetCurrentFolder(),DataFile.GetFilenameAlone()+".arff");
        Log.i(TAG,"Absolute FileName is:"+DataFile.GetFilename());
        // Initializing the window size and the interval
        WindowSize=200;
        SlidingInterval=50;
        //Reading the Sensor raw Data File
        SensorFileManager rawDataFile=new SensorFileManager(DataFile);
        //Defining xIndex, yIndex and zIndex in raw Datafile
        int xIndex=1;
        int yIndex=2;
        int zIndex=3;
        //Reading the total number of lines in the raw Data file
        int totalRowsInFile=rawDataFile.getNumberOfTotalSamples();
        StartIndex=1;
        EndIndex=totalRowsInFile;
        XSeries=rawDataFile.txtReadWindow(xIndex,1,totalRowsInFile);
        YSeries=rawDataFile.txtReadWindow(yIndex,1,totalRowsInFile);
        ZSeries=rawDataFile.txtReadWindow(zIndex,1,totalRowsInFile);
        MSeries=computeMagnitude(XSeries,YSeries,ZSeries);
        removeOutliersFromBeginAndEnd();
        EndIndex=XSeries.length; //Updating the EndIndex after rmeoving the outliers
    }
    /*
     * The below function computes the Magnitude of the X, Y and Z axis data
     */
    public static double [] computeMagnitude(double[]X, double []Y, double []Z)
    {
        double M[]= new double[X.length];
        for (int itr=0;itr<X.length;itr++)
        {
            M[itr]=Math.sqrt(X[itr]*X[itr]+Y[itr]*Y[itr]+Z[itr]*Z[itr]);
        }
        return M;
    }

    //ADD THIS FUNCTION
    /*
     * The below function is used to remove the outliers from the begining and the end.
     * As of now 10 percent of the begining and the end data is removed while processing the data.
     * Please refer Length-Length/10
     */
    public void removeOutliersFromBeginAndEnd()
    {
        int Length=XSeries.length;
        Log.i(TAG, "Before Removal - Length of Array -" +XSeries.length);
        int begin=Length/10;
        int end=(Length*4)/8;
        System.out.println("Removing "+begin+" Lines from the Begining and "+(Length-end)+" from the end");
        XSeries= Arrays.copyOfRange(XSeries, begin, Length-Length/10);
        YSeries=Arrays.copyOfRange(YSeries, begin, Length-Length/10);
        ZSeries=Arrays.copyOfRange(ZSeries, begin, Length-Length/10);
        MSeries=Arrays.copyOfRange(MSeries, begin, Length-Length/10);
        Log.i(TAG, "After Removal - Length of Array -" +XSeries.length);

    }
    /*
     *The below is the heart of this module which is used to extract features.
     * The features are extracted using the below code
     */
    public void extractFeatures() throws IOException
    {
        //Set the Column Index for X Y and Z according to the Data File
        int RowIndex=0,NumOfRows, FeatureCounter;
        int WindowCounter=1,StartWIndex,EndWIndex;
        FeatureManager SeriesX = new FeatureManager(Arrays.copyOfRange(XSeries, StartIndex, EndIndex));
        FeatureManager SeriesY = new FeatureManager(Arrays.copyOfRange(YSeries, StartIndex, EndIndex));
        FeatureManager SeriesZ = new FeatureManager(Arrays.copyOfRange(ZSeries, StartIndex, EndIndex));
        FeatureManager SeriesM = new FeatureManager(Arrays.copyOfRange(MSeries, StartIndex, EndIndex));

        SeriesX.smoothenSeries3Points();
        SeriesY.smoothenSeries3Points();
        SeriesZ.smoothenSeries3Points();
        SeriesM.smoothenSeries3Points();

        int totalRowsToBeProcessed=SeriesX.getLength();
        if(totalRowsToBeProcessed%SlidingInterval==0) NumOfRows=totalRowsToBeProcessed/SlidingInterval;
        else NumOfRows=(totalRowsToBeProcessed/SlidingInterval)+1;

        FeatureMatrix= new double [NumOfRows][TotalFeatures];
        // System.out.println("Feature Matrix Defined ----With the following Size ");
        //  System.out.println("TemplateManager(Extract Feature)--Size of the Feature Matrix - ROws: "+FeatureMatrix.length+ "and Columns"+FeatureMatrix[0].length);

        for ( StartWIndex=0;StartWIndex<totalRowsToBeProcessed;StartWIndex=StartWIndex+SlidingInterval)
        {
            // Determining endWIndex and making sure that last window contains all
            // remaining rows in case there is less than 200 rows
            if ((StartWIndex+WindowSize)<totalRowsToBeProcessed) EndWIndex=StartWIndex+WindowSize-1;
            else EndWIndex=totalRowsToBeProcessed;
            FeatureCounter=0;
            // Displaying the current WindowNo, its startIndex and EndIndex
            // System.out.println("Extract Features for the Window = " + WindowCounter);
            // System.out.println("startWIndex = "+ StartWIndex + " endWIndex = " + EndWIndex);
            int NOEWindow=EndWIndex-StartWIndex;
            //  System.out.println("Number Of Elements in Window:"+NOEWindow);
            double WindowX[] = new double[NOEWindow];
            double WindowY[] = new double[NOEWindow];
            double WindowZ[] = new double[NOEWindow];
            double WindowM[] = new double[NOEWindow];

            for (int index=StartWIndex, itr=0;index<EndWIndex;)
            {
                WindowX[itr]=SeriesX.getTimeSeries()[index];
                WindowY[itr]=SeriesY.getTimeSeries()[index];
                WindowZ[itr]=SeriesZ.getTimeSeries()[index];
                WindowM[itr++]=SeriesM.getTimeSeries()[index++];
            }
            FeatureManager FeaturesFromX = new FeatureManager(WindowX);
            FeatureManager FeaturesFromY = new FeatureManager(WindowY);
            FeatureManager FeaturesFromZ = new FeatureManager(WindowZ);
            FeatureManager FeaturesFromM = new FeatureManager(WindowM);

            for (int XItr=0;XItr<FeaturesFromX.getNumberOfFeaturesPerSeries();XItr++)
            {
                FeatureMatrix[RowIndex][FeatureCounter++]=FeaturesFromX.getAllTheFeatures()[XItr];
            }

            for (int YItr=0;YItr<FeaturesFromY.getNumberOfFeaturesPerSeries();YItr++)
            {
                FeatureMatrix[RowIndex][FeatureCounter++]=FeaturesFromY.getAllTheFeatures()[YItr];
            }


            for (int ZItr=0;ZItr<FeaturesFromZ.getNumberOfFeaturesPerSeries();ZItr++)
            {
                FeatureMatrix[RowIndex][FeatureCounter++]=FeaturesFromZ.getAllTheFeatures()[ZItr];
            }


            for (int MItr=0;MItr<FeaturesFromM.getNumberOfFeaturesPerSeries();MItr++)
            {
                FeatureMatrix[RowIndex][FeatureCounter++]=FeaturesFromM.getAllTheFeatures()[MItr];

            }
            RowIndex++;
            WindowCounter=WindowCounter+1;
        }


    }
    /*
     * The below function is used to normalize the featured.
     */
    public void normalizeFeatures() throws IOException
    {
        int NumberOfRows;
        NumberOfRows=FeatureMatrix.length;
        double TempArray[]= new double[NumberOfRows];
        double Min, Max, Differences;
        int NumberOfFeaturesToBeNormalized=55;
        for (int FeatureItr=0;FeatureItr<NumberOfFeaturesToBeNormalized;FeatureItr++)
        {
            for(int VectorItr=0;VectorItr<NumberOfRows;VectorItr++)
            {
                TempArray[VectorItr]=FeatureMatrix[VectorItr][FeatureItr];
            }
            Arrays.sort(TempArray);
            Min=TempArray[0];Max=TempArray[TempArray.length-1];
            Differences=Max-Min;


            for(int VectorItr=0;VectorItr<NumberOfRows;VectorItr++)
            {

                if(Differences==0)
                    FeatureMatrix[VectorItr][FeatureItr]= FeatureMatrix[VectorItr][FeatureItr]-Min;
                else
                    FeatureMatrix[VectorItr][FeatureItr]= (FeatureMatrix[VectorItr][FeatureItr]-Min)/Differences;
            }

        }

        for (int FeatureItr=0;FeatureItr<NumberOfFeaturesToBeNormalized;FeatureItr++)
        {
            for(int VectorItr=0;VectorItr<NumberOfRows;VectorItr++)
            {
                if(Double.isNaN(FeatureMatrix[VectorItr][FeatureItr]) || FeatureMatrix[VectorItr][FeatureItr]==0.0)
                // if(Double.isNaN(FeatureMatrix[VectorItr][FeatureItr]))
                {
                    FeatureMatrix[VectorItr][FeatureItr]=0.0025;
                    Log.i(TAG, "Found NaN or 0.0 here And replacing with 0.0025 : "+FeatureMatrix[VectorItr][FeatureItr]);
                }


            }
        }

    }
    /*
     * The below function uses the X, Y, Z and M series to create the Arff file for each raw data file
     */
    public void createArffFileForTraining() throws Exception
    {
        createFeatureFile();
        AndroidArffCreator ArffCreatorObject = new AndroidArffCreator(AndroidArffFile,true); // Only One file hence no need to append
        ArffCreatorObject.SetRelation(AndroidArffFile.GetFilename().replace(".", ""));
        ArffCreatorObject.SetAttributes(AttributesList);
        ArffCreatorObject.SetData(FeatureMatrix);
    }
    //Overloading of above function
    public void createArffFileForTraining(String genOrImp) throws Exception
    {
        try
        {
            createFeatureFile();
            AndroidArffCreator ArffCreatorObject = new AndroidArffCreator(AndroidArffFile,true); // Only One file hence no need to append
            ArffCreatorObject.SetRelation(AndroidArffFile.GetFilename().replace(".", ""));
            ArffCreatorObject.SetAttributes(AttributesList);
            ArffCreatorObject.SetData(FeatureMatrix,genOrImp);
            ArffCreatorObject.Close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }


    }
    /*
     * The below function is used to create the ArffFile for Testing File.
     */
    public void createArffFileForTesting()
    {
        try
        {
            createFeatureFile();
            AndroidArffCreator ArffCreatorObject = new AndroidArffCreator(AndroidArffFile,true); // Only One file hence no need to append
            ArffCreatorObject.SetRelation(AndroidArffFile.GetFilename().replace(".", ""));
            ArffCreatorObject.SetAttributes(AttributesList);
            ArffCreatorObject.SetData(FeatureMatrix);
            ArffCreatorObject.Close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

    }
    /*
     * The below function creates the feature file from X, Y, Z and M information device from the remove outliers function
     */
    public void createFeatureFile() throws Exception
    {
        extractFeatures();
        normalizeFeatures();

    }
}
