package com.Sensor.authentication;

import android.util.Log;

import com.Sensor.generalobjects.AndroidFileManager;
import com.Sensor.generalobjects.FileWriterObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Created by malz on 11/22/16.
 */
/*
 * The below class is helper for SEnsor code
 * The below code helps to seggregate the data and pass to the Feature MAnager. This will be used by the featur emanager code.
 */
public class SensorFileManager {
    private final String	TAG	= "SFM";
    public File FileName;
    public String FileType;
    private String DataFolder = "";
    public SensorFileManager(File FN, String DF)
    {
        this.DataFolder = DF;
        this.FileName= new File(DataFolder,FN.getName());
        Log.i(TAG, "Data folder is in sensor file manager is = "+DF+" and filename="+FileName.getAbsolutePath());
        //new File(Environment.getExternalStorageDirectory(), DataFolder);

    }

    public SensorFileManager(AndroidFileManager AFM)
    {
        this.DataFolder = AFM.GetCurrentFolder();
        this.FileName= new File(AFM.GetAbsoluteFilename());
        Log.i(TAG, "Data folder is in sensor file manager is = "+FileName);
        //new File(Environment.getExternalStorageDirectory(), DataFolder);

    }
    /*
    This will check if the string is double or not.
    This function especially parses the string passed into double. incase there is any alpha numberic then
    fase is return as shown in the below source code.
     */
    public static boolean isDouble(String Str)
    {
        try
        {
            @SuppressWarnings("unused")
            double DBL = Double.parseDouble(Str);
        }
        catch(NumberFormatException nfe)
        {
            // return false if all the characters are not double.
            return false;
        }
        return true;
    }

    @SuppressWarnings("resource")
    /*
     * the below function takes input of current column number and window size.
     * The function should return the double string with read values.
     * This function is not used in this project.
     */
    public double[] txtReadWindow(int column, int from, int windowSize) throws FileNotFoundException, IOException
    {
        int LineNumber=0, ElementIndex, ArrIndex=0;
        double ElementArray[] = new double[windowSize+1];
        BufferedReader bufRdr;
        bufRdr = new BufferedReader(new FileReader(FileName));
        String Line;
        //read each line of text file
        String Token;
        while((Line = bufRdr.readLine()) != null)
        {
            LineNumber++;
            if(LineNumber>=from && LineNumber<=(from+windowSize))
            {
                StringTokenizer st = new StringTokenizer(Line,",");
                ElementIndex=0;
                while (st.hasMoreTokens())
                {
                    //get next token and store it in the array if it is double and the desired column
                    Token =st.nextToken();
                    ElementIndex++;
                    if(isDouble(Token)&& ElementIndex==column)
                        ElementArray[ArrIndex++]=Double.parseDouble(Token);
                }
            }
            else if (LineNumber>(from+windowSize)) break;

        }

        return ElementArray;
    }
    /*
     Returns the number of total samples present in the raw data file
     */
    public int getNumberOfTotalSamples() throws FileNotFoundException, IOException
    {
        int LineNumber=0;
        @SuppressWarnings("resource")
        BufferedReader bufRdr = new BufferedReader(new FileReader(FileName));
        while(bufRdr.readLine()!= null)
        {
            LineNumber++;
        }
        return LineNumber;
    }
    /* write feature matrix to file
       public void writeMatrixToFile(double FVMatrix[][]) throws FileNotFoundException, IOException
       {
         String StrToWrite;
         int NRows=FVMatrix.length;
         int NCols=FVMatrix[0].length;
         System.out.println("FileManager--Size of the Feature Matrix - ROws: "+NRows+ "and Columns"+NCols);
         System.out.println("FileName -"+FileName);
         FileWriter fstream;
         BufferedWriter Writer=null;
         try{
               fstream = new FileWriter(FileName, false);
               Writer = new BufferedWriter(fstream);
            }
         catch(IOException e)
          {
            System.out.println("Exception: "+e);
          }

         for (int RowItr=0; RowItr<NRows ; RowItr++)
            {
                StrToWrite="";
                for(int ColItr=0;ColItr<NCols;ColItr++)
                 {
                    StrToWrite += String.valueOf(FVMatrix[RowItr][ColItr]+(((ColItr<NCols-1))?",":""));

                 }
            //    System.out.println("Line To Write "+StrToWrite);
                Writer.write(StrToWrite);
                Writer.write("\n");
              }
             Writer.close();

    }
       *************/
    /*
    This fucntion writes the matrix which is two dimentsional into the file on the disk
     */
    public void writeMatrixIntoFile(double[][] fvmatrix)
    {
        String Str = "";
        int NRow=fvmatrix.length;
        int NCol=fvmatrix[0].length;
//           float[][] FloatFeatureMatrix= new float[NRow][NCol];
//           for(int i=0;i<NRow;i++)
//           {
//          	 for(int j=0;j<NCol;j++)
//          	 {
//
//          		 FloatFeatureMatrix[i][j]= (float)fvmatrix[i][j];
//          	 }
//
//
//           }
//
        Log.i(TAG,"Feature Matrix with Rows = "+NRow+", NCols="+NCol+"Are Being Written to the Feature File"+FileName.getAbsolutePath());
        FileWriterObject FOBJ = new FileWriterObject(FileName);
        //   DecimalFormat doubleFormator = new DecimalFormat("0.000000");
        for (int i=0; i<NRow ; i++)
        {
            for(int j=0;j<NCol;j++)
            {
                Str += String.valueOf(fvmatrix[i][j]+(((j<NCol-1))?",":""));
                //       Str += ""+doubleFormator.format(fvmatrix[i][j])+(((j<NCol-1))?",":"");

            }
            FOBJ.SaveData(Str);
            Str = "";
        }
        FOBJ.CloseWriter();
    }

}
