package com.Sensor.authentication;

import android.util.Log;

import com.Sensor.generalobjects.AndroidFileManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by malz on 11/22/16.
 */
/*
 * The below class is not used in this project and hence can be ignored.
 * Please refer SensorsTemplateManagerR in authentication section.
 */
public class SensorsTemplateManager {
    private static final String	TAG	= null;
    //private File RawDataFile;
    private AndroidFileManager GlobalFileManager;
    private AndroidFileManager AndroidFeatureFile;
    private AndroidFileManager AndroidArffFile;
    //private File FeatureFile;
//private File ArffFile;
    boolean Training;
    int StartIndex, EndIndex;
    private final int WindowSize,SlidingInterval;
    private int TestSlidingInterval=500;
    private double FeatureMatrix[][];
    private String DataFolder = "";
    //private File root;
    private double XSeries[],YSeries[],ZSeries[], MSeries[];
    private final int TotalFeatures=56;
    private String [][]AttributesList={
            {"apiX", "numeric"}, {"energyX", "numeric"} ,{"fQuantileX", "numeric"} ,{"kurtosisX", "numeric"} ,{"maxX", "numeric"} ,{"meanX", "numeric"},{"minX", "numeric"},
            {"nopeaksX", "numeric"} ,{"noZeroCrosingsX", "numeric"} ,{"powerX", "numeric"} ,{"skewnessX", "numeric"} ,{"sQuantileX", "numeric"},{"stdX", "numeric"},{"thQuantileX", "numeric"},
            {"apiY", "numeric"}, {"energyY", "numeric"} ,{"fQuantileY", "numeric"} ,{"kurtosisY", "numeric"} ,{"maxY", "numeric"} ,{"meanY", "numeric"},{"minY", "numeric"},
            {"nopeaksY", "numeric"} ,{"noZeroCrosingsY", "numeric"} ,{"powerY", "numeric"} ,{"skewnessY", "numeric"} ,{"sQuantileY", "numeric"},{"stdY", "numeric"},{"thQuantileY", "numeric"},
            {"apiZ", "numeric"}, {"energyZ", "numeric"} ,{"fQuantileZ", "numeric"} ,{"kurtosisZ", "numeric"} ,{"maxZ", "numeric"} ,{"meanZ", "numeric"},{"minZ", "numeric"},
            {"nopeaksZ", "numeric"} ,{"noZeroCrosingsZ", "numeric"} ,{"powerZ", "numeric"} ,{"skewnessZ", "numeric"} ,{"sQuantileZ", "numeric"},{"stdZ", "numeric"},{"thQuantileZ", "numeric"},
            {"apiM", "numeric"}, {"energyM", "numeric"} ,{"fQuantileM", "numeric"} ,{"kurtosisM", "numeric"} ,{"maxM", "numeric"} ,{"meanM", "numeric"},{"minM", "numeric"},
            {"nopeaksM", "numeric"} ,{"noZeroCrosingsM", "numeric"} ,{"powerM", "numeric"} ,{"skewnessM", "numeric"} ,{"sQuantileM", "numeric"},{"stdM", "numeric"},{"thQuantileM", "numeric"},

            {"class", "{Genuine,Imposter}"}};
    //
    public SensorsTemplateManager(AndroidFileManager DataFile, boolean TrainingOrTest) throws FileNotFoundException, IOException
    {
        //	 Log.i(TAG, "Template manager is being created");
        this.GlobalFileManager = DataFile;
        this.Training=TrainingOrTest;
        if(TrainingOrTest)
        {

            Log.i(TAG,"Data Folder Location Set To DDDDDD  "+GlobalFileManager.GetCurrentFolder());
            AndroidFeatureFile= new AndroidFileManager(GlobalFileManager.GetCurrentFolder(), "TrainingFeatureFile.txt"); //new File(DataFolder,"Sensors/"+Username+"/TrainingFeatureFile.txt");
            AndroidArffFile=new AndroidFileManager(GlobalFileManager.GetCurrentFolder(), "Training.arff"); //new File(DataFolder, "Sensors/"+Username+"/Training.arff");
            Log.i(TAG,"Location of Training arff: "+AndroidArffFile.GetAbsoluteFilename());
        }
        else
        {
            Log.i(TAG,"Data Folder Location Set To DDDDDDFFFFF  "+DataFolder);
            this.Training=TrainingOrTest;
            AndroidFeatureFile= new AndroidFileManager(GlobalFileManager.GetCurrentFolder(), "TestingFeatureFile.txt"); //new File(DataFolder,"Sensors/"+Username+"/TrainingFeatureFile.txt");
            AndroidArffFile=new AndroidFileManager(GlobalFileManager.GetCurrentFolder(), "Testing.arff", false); //new File(DataFolder, "Sensors/"+Username+"/Training.arff");
            Log.i(TAG,"Created a name for the Testing Feature File  :"+ AndroidArffFile.GetAbsoluteFilename());
        }
        // Intializing the Size of Sliding Window and the Interval
        WindowSize=200;SlidingInterval=50;int xColIndex=1,yColIndex=2,zColIndex=3;
        SensorFileManager FileObject = new SensorFileManager(GlobalFileManager);
        int totalRowsInFile=FileObject.getNumberOfTotalSamples();
        StartIndex=1;
        EndIndex=totalRowsInFile;
        XSeries = FileObject.txtReadWindow(xColIndex, 1, totalRowsInFile);
        YSeries = FileObject.txtReadWindow(yColIndex, 1, totalRowsInFile);
        ZSeries = FileObject.txtReadWindow(zColIndex, 1, totalRowsInFile);
        MSeries = computeMagnitude(XSeries,YSeries,ZSeries);
        if(Training)
        {
            removeOutliersFromBeginAndEnd();
        }
    }

    public static double [] computeMagnitude(double[]X, double []Y, double []Z)
    {
        double M[]= new double[X.length];
        for (int itr=0;itr<X.length;itr++)
        {
            M[itr]=Math.sqrt(X[itr]*X[itr]+Y[itr]*Y[itr]+Z[itr]*Z[itr]);
        }
        return M;
    }

    //ADD THIS FUNCTION
    public void removeOutliersFromBeginAndEnd()
    {
        int Length=XSeries.length;
        Log.i(TAG, "Before Removal - Length of Array -" +XSeries.length);
        int begin=Length/8;
        int end=(Length*4)/8;
        System.out.println("Removing "+begin+" Lines from the Begining and "+(Length-end)+" from the end");
        XSeries= Arrays.copyOfRange(XSeries, begin, Length);
        YSeries=Arrays.copyOfRange(YSeries, begin, Length);
        ZSeries=Arrays.copyOfRange(ZSeries, begin, Length);
        MSeries=Arrays.copyOfRange(MSeries, begin, Length);
        Log.i(TAG, "After Removal - Length of Array -" +XSeries.length);

    }


    public void extractFeatures() throws IOException
    {
        //Set the Column Index for X Y and Z according to the Data File
        int RowIndex=0,NumOfRows, FeatureCounter;
        int WindowCounter=1,StartWIndex,EndWIndex;
        FeatureManager SeriesX = new FeatureManager(Arrays.copyOfRange(XSeries, StartIndex, EndIndex));
        FeatureManager SeriesY = new FeatureManager(Arrays.copyOfRange(YSeries, StartIndex, EndIndex));
        FeatureManager SeriesZ = new FeatureManager(Arrays.copyOfRange(ZSeries, StartIndex, EndIndex));
        FeatureManager SeriesM = new FeatureManager(Arrays.copyOfRange(MSeries, StartIndex, EndIndex));

        SeriesX.smoothenSeries3Points();
        SeriesY.smoothenSeries3Points();
        SeriesZ.smoothenSeries3Points();
        SeriesM.smoothenSeries3Points();

        int totalRowsToBeProcessed=SeriesX.getLength();
        if(totalRowsToBeProcessed%SlidingInterval==0) NumOfRows=totalRowsToBeProcessed/SlidingInterval;
        else NumOfRows=(totalRowsToBeProcessed/SlidingInterval)+1;

        FeatureMatrix= new double [NumOfRows][TotalFeatures];
        // System.out.println("Feature Matrix Defined ----With the following Size ");
        //  System.out.println("TemplateManager(Extract Feature)--Size of the Feature Matrix - ROws: "+FeatureMatrix.length+ "and Columns"+FeatureMatrix[0].length);

        for ( StartWIndex=0;StartWIndex<totalRowsToBeProcessed;StartWIndex=StartWIndex+SlidingInterval)
        {
            // Determining endWIndex and making sure that last window contains all
            // remaining rows in case there is less than 200 rows
            if ((StartWIndex+WindowSize)<totalRowsToBeProcessed) EndWIndex=StartWIndex+WindowSize-1;
            else EndWIndex=totalRowsToBeProcessed;
            FeatureCounter=0;
            // Displaying the current WindowNo, its startIndex and EndIndex
            // System.out.println("Extract Features for the Window = " + WindowCounter);
            // System.out.println("startWIndex = "+ StartWIndex + " endWIndex = " + EndWIndex);
            int NOEWindow=EndWIndex-StartWIndex;
            //  System.out.println("Number Of Elements in Window:"+NOEWindow);
            double WindowX[] = new double[NOEWindow];
            double WindowY[] = new double[NOEWindow];
            double WindowZ[] = new double[NOEWindow];
            double WindowM[] = new double[NOEWindow];

            for (int index=StartWIndex, itr=0;index<EndWIndex;)
            {
                WindowX[itr]=SeriesX.getTimeSeries()[index];
                WindowY[itr]=SeriesY.getTimeSeries()[index];
                WindowZ[itr]=SeriesZ.getTimeSeries()[index];
                WindowM[itr++]=SeriesM.getTimeSeries()[index++];
            }
            FeatureManager FeaturesFromX = new FeatureManager(WindowX);
            FeatureManager FeaturesFromY = new FeatureManager(WindowY);
            FeatureManager FeaturesFromZ = new FeatureManager(WindowZ);
            FeatureManager FeaturesFromM = new FeatureManager(WindowM);

            for (int XItr=0;XItr<FeaturesFromX.getNumberOfFeaturesPerSeries();XItr++)
            {
                FeatureMatrix[RowIndex][FeatureCounter++]=FeaturesFromX.getAllTheFeatures()[XItr];
            }

            for (int YItr=0;YItr<FeaturesFromY.getNumberOfFeaturesPerSeries();YItr++)
            {
                FeatureMatrix[RowIndex][FeatureCounter++]=FeaturesFromY.getAllTheFeatures()[YItr];
            }


            for (int ZItr=0;ZItr<FeaturesFromZ.getNumberOfFeaturesPerSeries();ZItr++)
            {
                FeatureMatrix[RowIndex][FeatureCounter++]=FeaturesFromZ.getAllTheFeatures()[ZItr];
            }


            for (int MItr=0;MItr<FeaturesFromM.getNumberOfFeaturesPerSeries();MItr++)
            {
                FeatureMatrix[RowIndex][FeatureCounter++]=FeaturesFromM.getAllTheFeatures()[MItr];

            }
            RowIndex++;
            WindowCounter=WindowCounter+1;
        }


    }

    public void normalizeFeatures() throws IOException
    {
        int NumberOfRows;
        NumberOfRows=FeatureMatrix.length;
        double TempArray[]= new double[NumberOfRows];
        double Min, Max, Differences;
        int NumberOfFeaturesToBeNormalized=55;
        for (int FeatureItr=0;FeatureItr<NumberOfFeaturesToBeNormalized;FeatureItr++)
        {
            for(int VectorItr=0;VectorItr<NumberOfRows;VectorItr++)
            {
                TempArray[VectorItr]=FeatureMatrix[VectorItr][FeatureItr];
            }
            Arrays.sort(TempArray);
            Min=TempArray[0];Max=TempArray[TempArray.length-1];
            Differences=Max-Min;


            for(int VectorItr=0;VectorItr<NumberOfRows;VectorItr++)
            {

                if(Differences==0)
                    FeatureMatrix[VectorItr][FeatureItr]= FeatureMatrix[VectorItr][FeatureItr]-Min;
                else
                    FeatureMatrix[VectorItr][FeatureItr]= (FeatureMatrix[VectorItr][FeatureItr]-Min)/Differences;
            }

        }

        for (int FeatureItr=0;FeatureItr<NumberOfFeaturesToBeNormalized;FeatureItr++)
        {
            for(int VectorItr=0;VectorItr<NumberOfRows;VectorItr++)
            {
                if(Double.isNaN(FeatureMatrix[VectorItr][FeatureItr]) || FeatureMatrix[VectorItr][FeatureItr]==0.0)
                // if(Double.isNaN(FeatureMatrix[VectorItr][FeatureItr]))
                {
                    FeatureMatrix[VectorItr][FeatureItr]=0.0025;
                    Log.i(TAG, "Found NaN or 0.0 here And replacing with 0.0025 : "+FeatureMatrix[VectorItr][FeatureItr]);
                }


            }
        }

    }

    //REPLACE THIS AS WELL
    public void createFeatureFile() throws IOException
    {

        extractFeatures(); //Extracting Features and Intializing FeatureMatrix
        System.out.println("NormaliZing the Features After Creating Feature Matrix");
        normalizeFeatures(); //Normalize the features
        Log.i(TAG, "Data folder here is something like this: "+DataFolder);
        // SensorFileManager FileWriter = new SensorFileManager(AndroidFeatureFile);
        //FileWriter.writeMatrixIntoFile(FeatureMatrix);

    }

    public void createArffFileForTraining() throws Exception
    {
        //Creating the feature matrix from the raw data taking XXX samples each time.
        createFeatureFile();
        AndroidArffCreator ArffCreatorObject = new AndroidArffCreator(AndroidArffFile,!Training); // Only One file hence no need to append
        ArffCreatorObject.SetRelation(AndroidArffFile.GetFilename().replace(".", ""));
        ArffCreatorObject.SetAttributes(AttributesList);
        ArffCreatorObject.SetData(FeatureMatrix);
        Log.i(TAG, "Creating arff file here: "+AndroidArffFile.GetAbsoluteFilename());
        // Append the Imposters
        ArffCreatorObject.appendImposter();


    }

    public void createArffFileForTesting() throws Exception
    {

        SensorFileManager FileObject = new SensorFileManager(GlobalFileManager);
        int totalSamples=FileObject.getNumberOfTotalSamples();
        StartIndex=totalSamples-TestSlidingInterval;
        EndIndex=totalSamples;
        // AndroidFeatureFile.SetFilename("Features"+AndroidArffFile.GetFilename());
        createFeatureFile();
        AndroidArffCreator ArffCreatorObject = new AndroidArffCreator(AndroidArffFile, false);//Destrying the previous one and creating a new test file
        ArffCreatorObject.SetRelation(AndroidArffFile.GetFilename().replace(".", ""));
        ArffCreatorObject.SetAttributes(AttributesList);
        ArffCreatorObject.SetData(FeatureMatrix);
        Log.i(TAG, "Creating Test arff file here: "+AndroidArffFile.GetAbsoluteFilename());

    }
}
