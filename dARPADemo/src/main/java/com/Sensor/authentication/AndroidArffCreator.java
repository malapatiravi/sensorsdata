package com.Sensor.authentication;

import android.os.Environment;
import android.util.Log;

import com.Sensor.generalobjects.AndroidFileManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by malz on 11/22/16.
 * This module is used to create the Arff file.
 * Arff file usualy consists of the features list and the feature vectors followed.
 *
 */
public class AndroidArffCreator extends AndroidFileManager {
    /*
     *The below are the locla intialization variable required for the Android Arff Creator.
     *
     */
    //private String FileLocation = "";
    //private String Filename = "";
    private boolean RelationSet;
    private boolean AttributeSet;
    private int TotalAttribs;
    private String[] AttribsTypeList;
    private String TAG = "ARRF";
    File ImposterFile;
    private final String RelationTag 		= "@relation ";
    private final String AttributeTag 	= "@attribute ";
    private final String DataTag 				= "@data";
    private final String[] ValidDataTypes = {"numeric", "string"};
    AndroidArffCreator()
    {
        /*
         *  This is the constructor, this is not used for any practical use and hence can be ignored while modifying the code.
         */
        super(null);
        try
        {
            throw new Exception("File location and name must be provided");
        } catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public AndroidArffCreator(AndroidFileManager  AFMObject)
    {
        /*
         * This is another Constructor and can be used to intialize using the Android File Manager.
         * Android file manager is the user File class which is used to write and read from the file.
         * Before modifying any of the code, please check the AndroidFileManager.
         */
        super(AFMObject);
        /*
         * Her though we are initializing the imposter file this is not used in this project and hence the initialization can be ignored.
         *
         */
        ImposterFile =  new File(Environment.getExternalStorageDirectory(), "Imposter/GlobalImposter.txt");
        RelationSet = false;
        AttributeSet = false;
        TotalAttribs = 0;
    }
    /*
     * The below constructor is used to provide Android File Manager object.
     * You can observe the Imposter File is commented which means it is not used anywhere in this project.
     */
    public AndroidArffCreator(AndroidFileManager AFMObject, boolean Append)
    {
        super(AFMObject);
        //ImposterFile =  new File(Environment.getExternalStorageDirectory(), "DataFolder/Imposter/GlobalImposter.txt");
        RelationSet = false;
        AttributeSet = false;
        TotalAttribs = 0;
    }

    /*
     * The below constructor is not used anythwere in this project and hence can be ignred.
     */
    public AndroidArffCreator(String Filename, String FolderLocation)
    {
        super(Filename, FolderLocation);
        RelationSet = false;
        AttributeSet = false;
        TotalAttribs = 0;
    }
    /*
     * The below constructor is not used anywhere in this project and hence can be ignored.
     */
    public AndroidArffCreator(String Filename, String FolderLocation, boolean Append)
    {
        super(Filename, FolderLocation, Append);
        RelationSet = false;
        AttributeSet = false;
        TotalAttribs = 0;
    }
    /*
     * The below function is used to check if the datatype is valid or nor.
     */
    public boolean IsValidDataType(String DT)
    {
        if (Arrays.asList(ValidDataTypes).contains(DT))
            return true;
        else
            return false;
    }
    /*
     * Relation tag is set
     */
    public void SetRelation(String RelationVal)
    {
        //String CurrentLine = RelationTag+EncloseInQuotes(RelationVal);
        String CurrentLine = RelationTag+"  "+RelationVal;
        this.SaveData(CurrentLine);
        RelationSet = true;
    }
    /*
     * Feature attribute is set which is part of the Arff File
     */
    public void SetAttributes(String [][]Attribs)
    {
        try
        {
            if (!RelationSet) throw new Exception("Relation is not set!");
            if (Attribs.length < 1) 									throw new Exception("Attributes array is empty");
            if (Attribs[0].length != 2)						 		throw new Exception("Attributes variable must be <attribute-name> <datatype> pair");
        } catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        TotalAttribs = Attribs.length;
        String CurrentLine = "";
        AttribsTypeList = new String[Attribs.length];
        this.SaveData("");
        for (int i =0; i < TotalAttribs; i++)
        {
            AttribsTypeList[i] = Attribs[i][1];
            try
            {
                if ((!IsValidDataType(AttribsTypeList[i])) && (Attribs[i][0]!="class")) throw new Exception(AttribsTypeList[i]+" is not valid data type. Use: "+Arrays.toString(ValidDataTypes));
            } catch (Exception e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            //CurrentLine = AttributeTag+EncloseInQuotes(Attribs[i][0])+" "+Attribs[i][1];
            CurrentLine = AttributeTag+Attribs[i][0]+" "+Attribs[i][1];
            /*
             * Saving the data in the file
             */
            this.SaveData(CurrentLine);
        }
        AttributeSet = true;
    }
    /*
     * Two dimensional data matrix can also be written using the below function. This is used rarely.
     *
     */
    public void SetData(String[][] Data) throws Exception
    {
        Log.i(TAG, "Data[0].length = "+Data[0].length+" TotalAttribs ="+TotalAttribs);
        if (!AttributeSet) 											throw new Exception("Attributes are not set!");
        if (Data.length < 1) 										throw new Exception("Given data is empty");
        if (Data[0].length != (TotalAttribs)) 		throw new Exception("Total attributes in header does not match with provided in data section"+TotalAttribs);
        this.SaveData("");
        this.SaveData(DataTag);
        String CurrentLine;
        for (int i =0; i<Data.length; i++)
        {
            CurrentLine = "";
            for (int j=0; j<Data[i].length; j++)
            {
                //	CurrentLine += ((j==0)?"":",")+((AttribsTypeList[j]=="STRING")?EncloseInQuotes(Data[i][j]):Data[i][j]);
                CurrentLine += ((j==0)?"":",")+Data[i][j];
            }
            this.SaveData(CurrentLine);
        }
    }
    /*
     * The below function is used to write single line of feature vector.
     */
    public void SetData(String[] Data1)
    {
        String[][] Data = new String[Data1.length][TotalAttribs];
        for (int i = 0; i < Data1.length; i++)
        {
            if (!Data[i].equals(null))
            {
                String[] CurrentLine = Data1[i].split(",");
                Log.i(TAG, "i = " + i +"TotalAttribs = "+TotalAttribs+" CurrentLine: "+Arrays.toString(CurrentLine));
                for (int j =0; j < CurrentLine.length; j++)
                {
                    Data[i][j] = CurrentLine[j];
                }
            }
        }
        try
        {
            this.SetData(Data);
        } catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    // Overloaded SetData Function for Double Type Feature Matrix

    public void SetData(double[][] Data) throws Exception
    {
        //DecimalFormat doubleFormator = new DecimalFormat("0.000000");
        Log.i(TAG, "Data[0].length = "+Data[0].length+" TotalAttribs ="+TotalAttribs+" number of rows "+Data.length);
        if (!AttributeSet) 											throw new Exception("Attributes are not set!");
        if (Data.length < 1) 										throw new Exception("Given data is empty");
        if (Data[0].length != (TotalAttribs-1)) 		throw new Exception("Total attributes in header does not match with provided in data section");
        this.SaveData("");
        this.SaveData(DataTag);
        String CurrentLine;
        for (int i =0; i<Data.length; i++)
        {
            CurrentLine = "";
            for (int j=0; j<Data[i].length; j++)
            {
                CurrentLine += ((j==0)?"":",")+String.valueOf(Data[i][j]);;
                //CurrentLine += ((j==0)?"":",")+doubleFormator.format(Data[i][j]);;

            }
            this.SaveData(CurrentLine+",Genuine");
        }
    }
    public void SetData(double[][] Data, String genOrImp) throws Exception
    {
        //DecimalFormat doubleFormator = new DecimalFormat("0.000000");
        Log.i(TAG, "Data[0].length = "+Data[0].length+" TotalAttribs ="+TotalAttribs+" number of rows "+Data.length);
        if (!AttributeSet) 											throw new Exception("Attributes are not set!");
        if (Data.length < 1) 										throw new Exception("Given data is empty");
        if (Data[0].length != (TotalAttribs-1)) 		throw new Exception("Total attributes in header does not match with provided in data section");
        this.SaveData("");
        this.SaveData(DataTag);
        String CurrentLine;
        for (int i =0; i<Data.length; i++)
        {
            CurrentLine = "";
            for (int j=0; j<Data[i].length; j++)
            {
                CurrentLine += ((j==0)?"":",")+String.valueOf(Data[i][j]);;
                //CurrentLine += ((j==0)?"":",")+doubleFormator.format(Data[i][j]);;

            }
            this.SaveData(CurrentLine+","+genOrImp);
        }
    }
    // Added code to Append the Imposters run time

    public void appendImposter() throws IOException
    {
        BufferedReader ImposterReader = new BufferedReader(new FileReader(ImposterFile));
        Log.i(TAG, "Appending Imposters From the global Imposter File viz. : "+ImposterFile.getAbsolutePath());
        String CurrentLine;
        while((CurrentLine = ImposterReader.readLine()) != null) {
            this.SaveData(CurrentLine);
        }
        ImposterReader.close();

    }

    public boolean IsRelationSet()
    {
        return RelationSet;
    }

    public boolean IsAttributeSet()
    {
        return AttributeSet;
    }
    @Override
    public void Close()
    {
        this.CloseWriter();
    }
}
